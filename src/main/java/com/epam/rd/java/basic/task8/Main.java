package com.epam.rd.java.basic.task8;

import com.epam.rd.java.basic.task8.controller.*;
import com.epam.rd.java.basic.task8.entities.Sorter;


public class Main {
	
	static void main(String[] args) throws Exception {
		if (args.length != 1) {
			return;
		}
		
		String xmlFileName = args[0];
		System.out.println("Input ==> " + xmlFileName);
		
		////////////////////////////////////////////////////////
		// DOM
		////////////////////////////////////////////////////////
		
		// get container
		DOMController domController = new DOMController(xmlFileName);
		// PLACE YOUR CODE HERE
		domController.parse();
		// sort (case 1)
		// PLACE YOUR CODE HERE
		Sorter.setSortByName(domController.getRoot());
		// save
		String outputXmlFile = "output.dom.xml";
		// PLACE YOUR CODE HERE
		domController.saveToXML(outputXmlFile);
		////////////////////////////////////////////////////////
		// SAX
		////////////////////////////////////////////////////////
		
		// get
		SAXController saxController = new SAXController(xmlFileName);
		// PLACE YOUR CODE HERE
		saxController.parse();
		// sort  (case 2)
		// PLACE YOUR CODE HERE
		Sorter.setSortBySoilType(saxController.getRoot());
		// save
		outputXmlFile = "output.sax.xml";
		// PLACE YOUR CODE HERE
		saxController.saveToXML(outputXmlFile);
		////////////////////////////////////////////////////////
		// StAX
		////////////////////////////////////////////////////////
		
		// get
		STAXController staxController = new STAXController(xmlFileName);
		// PLACE YOUR CODE HERE
		staxController.parse();
		// sort  (case 3)
		// PLACE YOUR CODE HERE
		Sorter.setSortByAverageLength(saxController.getRoot());
		// save
		outputXmlFile = "output.stax.xml";
		// PLACE YOUR CODE HERE
		saxController.saveToXML(outputXmlFile);
	}

}
