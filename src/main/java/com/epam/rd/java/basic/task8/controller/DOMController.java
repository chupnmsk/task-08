package com.epam.rd.java.basic.task8.controller;


import com.epam.rd.java.basic.task8.controller.exceptions.DOMControllerException;
import com.epam.rd.java.basic.task8.entities.*;
import static com.epam.rd.java.basic.task8.util.XML.*;

import com.epam.rd.java.basic.task8.util.xml.builder.XMLBuilder;
import com.epam.rd.java.basic.task8.util.validator.ValidateException;
import com.epam.rd.java.basic.task8.util.validator.ValidatorAgainstXSD;
import com.epam.rd.java.basic.task8.util.xml.builder.XMLBuilderException;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;


/**
 * Controller for DOM parser.
 */
public class DOMController {
	private final String xmlFileName;

	private static final Flowers root = new Flowers();

	public Flowers getRoot() {
		return root;
	}

	public DOMController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}

	public void saveToXML(String xmlFileName) throws XMLBuilderException {
		Document document = XMLBuilder.getDocument(root);
		XMLBuilder.saveToXML(document,xmlFileName);
	}

	public void parse() throws DOMControllerException {
		Document document;
		try {
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			factory.setNamespaceAware(true);
			factory.setFeature("http://apache.org/xml/features/disallow-doctype-decl", true);
			factory.setFeature("http://xml.org/sax/features/external-general-entities", false);

			DocumentBuilder docBuilder = factory.newDocumentBuilder();

			ValidatorAgainstXSD.validate(xmlFileName,INPUT_XSD);
			document = docBuilder.parse(xmlFileName);
		} catch (ParserConfigurationException | SAXException | IOException | ValidateException e) {
			throw new DOMControllerException(e.getMessage(),e);
		}

		Node rootNode = document.getDocumentElement();

		root.setAttribute(getAttribute(rootNode));
		NodeList rootList = rootNode.getChildNodes();

		for (int i = 0; i < rootList.getLength(); i++) {
			if (rootList.item(i).getNodeName().equals(FLOWER_TAG)) {
				NodeList flowersList = rootList.item(i).getChildNodes();

				Flower flower = parseFlower(flowersList);

				root.addChild(flower);
			}
		}
	}


	private static Flower parseFlower(NodeList flowersList) {
		Flower flower = new Flower();

		for (int j = 0; j < flowersList.getLength(); j++) {
			Node flowerNode = flowersList.item(j);

			if (flowerNode.getNodeType() != Node.TEXT_NODE) {
				switch (flowerNode.getNodeName()) {
					case FLOWER_NAME:
						flower.setName(flowerNode.getTextContent());
						break;
					case FLOWER_SOIL:
						flower.setSoil(flowerNode.getTextContent());
						break;
					case FLOWER_ORIGIN:
						flower.setOrigin(flowerNode.getTextContent());
						break;
					case FLOWER_MULTIPLYING:
						flower.setMultiplying(flowerNode.getTextContent());
						break;
					case VISUAL_PARAMETERS_TAG:
						parseVisualParameters(flowerNode, flower);

						break;
					case GROWING_TIPS_TAG:
						parseGrowingTips(flowerNode, flower);
						break;
					default:
				}
			}
		}
		return flower;
	}

	private static void parseVisualParameters(Node flowerNode, Flower flower) {
		VisualParameters visPar = flower.getVisualParameters();

		NodeList visualParameters = flowerNode.getChildNodes();

		for (int k = 0; k < visualParameters.getLength(); k++) {
			Node visualParam = visualParameters.item(k);

			switch (visualParam.getNodeName()) {
				case VIS_PARAM_STEAM_COLOUR:
					visPar.setStemColour(visualParam.getTextContent());
					break;
				case VIS_PARAM_LEAF_COLOUR:
					visPar.setLeafColour(visualParam.getTextContent());
					break;

				case VIS_PARAM_AVELENFLOWER:
					VisualParameters.AveLenFlower aveLenFlower =
							flower.getVisualParameters().getAveLenFlower();

					aveLenFlower.setAttribute(visualParam.getAttributes().item(0).toString());
					aveLenFlower.setContent(visualParam.getTextContent());
					break;
				default:
			}
		}
	}

	private static void parseGrowingTips(Node flowerNode, Flower flower) {
		GrowingTips groTips = flower.getGrowingTips();

		NodeList growingTips = flowerNode.getChildNodes();

		for (int k = 0; k < growingTips.getLength(); k++) {
			Node currentNode = growingTips.item(k);
			switch (currentNode.getNodeName()) {
				case GRO_TIPS_TEMPERATURE:
					GrowingTips.Temperature temperature = groTips.getTemperature();

					temperature.setAttribute(currentNode.getAttributes().item(0).toString());
					temperature.setContent(currentNode.getTextContent());
					break;
				case GRO_TIPS_LIGHTING:
					GrowingTips.Lighting lighting = groTips.getLighting();
					lighting.setAttribute(currentNode.getAttributes().item(0).toString());

					break;
				case GRO_TIPS_WATERING:
					GrowingTips.Watering watering = groTips.getWatering();

					watering.setAttribute(currentNode.getAttributes().item(0).toString());
					watering.setContent(currentNode.getTextContent());
					break;
				default:
			}
		}
	}

	private static String getAttribute(Node  node) {
		StringBuilder sb = new StringBuilder();
		NamedNodeMap attribute = node.getAttributes();

		int attributeCount = attribute.getLength();

		for (int i = 0; i < attributeCount; i++) {
			sb.append(attribute.item(i)).append("\n");
		}
		// delete redundant "\n" in end of string
		return sb.toString().trim();
	}
}
