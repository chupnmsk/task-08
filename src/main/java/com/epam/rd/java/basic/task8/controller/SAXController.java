package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.controller.exceptions.SAXControllerException;
import com.epam.rd.java.basic.task8.entities.Flower;
import com.epam.rd.java.basic.task8.entities.Flowers;
import com.epam.rd.java.basic.task8.entities.GrowingTips;
import com.epam.rd.java.basic.task8.entities.VisualParameters;
import com.epam.rd.java.basic.task8.util.validator.ValidateException;
import com.epam.rd.java.basic.task8.util.validator.ValidatorAgainstXSD;
import com.epam.rd.java.basic.task8.util.xml.builder.XMLBuilder;
import com.epam.rd.java.basic.task8.util.xml.builder.XMLBuilderException;
import org.w3c.dom.Document;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import java.io.IOException;

import static com.epam.rd.java.basic.task8.util.XML.*;
/**
 * Controller for SAX parser.
 */
public class SAXController extends DefaultHandler {
	private final String xmlFileName;

	private Flowers root;
	private Flower flower;

	private String currentTag;
	private Attributes tagAttribute;

	public SAXController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}

	public Flowers getRoot() {
		return root;
	}

	public void parse() throws SAXControllerException {
		try {
			SAXParserFactory factory = SAXParserFactory.newInstance();
			factory.setFeature("http://apache.org/xml/features/disallow-doctype-decl", true);

			SAXParser parser = factory.newSAXParser();

			ValidatorAgainstXSD.validate(xmlFileName,INPUT_XSD);

			parser.parse(xmlFileName,this);
		} catch (ParserConfigurationException | SAXException | IOException | ValidateException e) {
			throw new SAXControllerException(e.getMessage(),e);
		}
	}

	public void saveToXML(String xmlFileName) throws XMLBuilderException {
		Document document = XMLBuilder.getDocument(root);
		XMLBuilder.saveToXML(document,xmlFileName);
	}

	private static String getAttribute(Attributes attribute){
		StringBuilder sb = new StringBuilder();
		int attributeCount = attribute.getLength();

		for (int i = 0; i < attributeCount; i++) {
			sb.append(attribute.getQName(i))
					.append("=\"")
					.append(attribute.getValue(i))
					.append("\"")
					.append("\n");
		}
		// delete redundant "\n" in end of string
		return sb.toString().trim();
	}


	@Override
	public void startDocument() {
		root = new Flowers();
	}

	@Override
	public void startElement (String uri, String localName, String qName, Attributes attributes) {
		if(ROOT.equals(qName) && attributes != null){
			root.setAttribute(getAttribute(attributes));
		}
		currentTag = qName;
		tagAttribute = attributes;
	}

	@Override
	public void endElement (String uri, String localName, String qName) {
		if (FLOWER_TAG.equals(qName)) {
			root.addChild(flower);
		}

		if(qName.equals(GRO_TIPS_LIGHTING)){
			currentTag = qName;
			return;
		}

		currentTag = null;
		tagAttribute = null;
	}

	@Override
	public void characters (char[] ch, int start, int length) {
		String tagContent = String.valueOf(ch, start, length);

		if(currentTag == null){
			return;
		}

		switch (currentTag){
			//Create and set <flower>
			case FLOWER_TAG: flower = new Flower();
				break;
			case FLOWER_NAME: flower.setName(tagContent);
				break;
			case FLOWER_SOIL: flower.setSoil(tagContent);
				break;
			case FLOWER_ORIGIN: flower.setOrigin(tagContent);
				break;
			case FLOWER_MULTIPLYING: flower.setMultiplying(tagContent);
				break;

			//set <visualParameters>
			case VIS_PARAM_STEAM_COLOUR: flower.getVisualParameters().setStemColour(tagContent);
				break;
			case VIS_PARAM_LEAF_COLOUR: flower.getVisualParameters().setLeafColour(tagContent);
				break;
			case VIS_PARAM_AVELENFLOWER:
				VisualParameters.AveLenFlower aLFlower = flower.getVisualParameters().getAveLenFlower();

				aLFlower.setAttribute(getAttribute(tagAttribute));
				aLFlower.setContent(tagContent);
				break;

			//set <growingTips>
			case GRO_TIPS_TEMPERATURE:
				GrowingTips.Temperature temperature = flower.getGrowingTips().getTemperature();

				temperature.setAttribute(getAttribute(tagAttribute));
				temperature.setContent(tagContent);
				break;
			case GRO_TIPS_LIGHTING:
				GrowingTips.Lighting lighting = flower.getGrowingTips().getLighting();

				lighting.setAttribute(getAttribute(tagAttribute));
				break;
			case GRO_TIPS_WATERING:
				GrowingTips.Watering watering = flower.getGrowingTips().getWatering();

				watering.setContent(tagContent);
				watering.setAttribute(getAttribute(tagAttribute));
				break;
			default:
		}

	}
}


