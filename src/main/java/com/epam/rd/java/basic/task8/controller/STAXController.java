package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.entities.Flower;
import com.epam.rd.java.basic.task8.entities.Flowers;
import com.epam.rd.java.basic.task8.entities.GrowingTips;
import com.epam.rd.java.basic.task8.entities.VisualParameters;
import com.epam.rd.java.basic.task8.util.validator.ValidateException;
import com.epam.rd.java.basic.task8.util.validator.ValidatorAgainstXSD;
import com.epam.rd.java.basic.task8.util.xml.builder.XMLBuilder;
import com.epam.rd.java.basic.task8.util.xml.builder.XMLBuilderException;
import org.w3c.dom.Document;
import org.xml.sax.helpers.DefaultHandler;

import java.util.Iterator;
import javax.xml.stream.*;
import javax.xml.stream.events.*;
import javax.xml.transform.stream.StreamSource;

import static com.epam.rd.java.basic.task8.util.XML.*;

/**
 * Controller for StAX parser.
 */
public class STAXController extends DefaultHandler {

    private final String xmlFileName;

    private Flowers root;
    private Flower flower;

    private String currentTag = "";
    private String tagAttribute = "";
    private String tagContent = "";

    public STAXController(String xmlFileName) {
        this.xmlFileName = xmlFileName;
    }

    private XMLEventReader getReader() throws XMLStreamException {
        XMLInputFactory factory = XMLInputFactory.newInstance();
            factory.setProperty(XMLInputFactory.SUPPORT_DTD, false);

        factory.setProperty(XMLInputFactory.IS_NAMESPACE_AWARE, false);

        return factory.createXMLEventReader(new StreamSource(xmlFileName));
    }

    public Flowers getRoot() {
        return root;
    }

    public void parse() throws XMLStreamException, ValidateException {
		ValidatorAgainstXSD.validate(xmlFileName,INPUT_XSD);

        XMLEventReader reader = getReader();
        //tag handler
        while (reader.hasNext()) {
            XMLEvent event = reader.nextEvent();

            if(event.isCharacters()){
                setTagContent(event);
            } else if(event.isStartDocument()){
                startDocument();
            } else if (event.isStartElement()){
                startElement(event);
            } else if (event.isEndElement()) {
                endElement(event);
            }
        }
    }

    @Override
    public void startDocument() {
        root = new Flowers();
    }

    public void startElement (XMLEvent event) {
        StartElement startElement = event.asStartElement();
        tagAttribute = getAttribute(startElement);
        currentTag = startElement.getName().getLocalPart();

        if (ROOT.equals(currentTag)) {
            root.setAttribute(tagAttribute);
        }
        if (FLOWER_TAG.equals(currentTag)) {
            flower = new Flower();
        }

    }

    public void endElement (XMLEvent event) {
        EndElement endElement = event.asEndElement();
        String endTag = endElement.getName().getLocalPart();

        if (FLOWER_TAG.equals(endTag)) {
            root.addChild(flower);
        }

        if (currentTag != null && flower != null) {
            fillTag(flower, currentTag, tagContent, tagAttribute);
        }

        currentTag = null;
        tagContent = null;
    }

    public void setTagContent(XMLEvent event) {
        Characters characters = event.asCharacters();
        tagContent = characters.getData();
    }



    public void saveToXML(String xmlFileName) throws XMLBuilderException {
		Document document = XMLBuilder.getDocument(root);
		XMLBuilder.saveToXML(document,xmlFileName);
	}

    private static String getAttribute(StartElement event) {
        StringBuilder sb = new StringBuilder();
        Iterator<Attribute> iterator = event.getAttributes();

        while (iterator.hasNext()) {
            Attribute attribute = iterator.next();
            String attr = attribute.toString().replace("'",""); //!
            sb.append(attr).append("\n");
        }
        // delete redundant "\n" in end of string
        return sb.toString().trim();
    }

    private void fillTag(Flower flower, String currentTag, String tagContent, String tagAttribute) {
        switch (currentTag) {
            case FLOWER_NAME:
                flower.setName(tagContent);
                break;
            case FLOWER_SOIL:
                flower.setSoil(tagContent);
                break;
            case FLOWER_ORIGIN:
                flower.setOrigin(tagContent);
                break;
            case FLOWER_MULTIPLYING:
                flower.setMultiplying(tagContent);
                break;

            //Create and set <visualParameters>
            case VIS_PARAM_STEAM_COLOUR:
                flower.getVisualParameters().setStemColour(tagContent);
                break;
            case VIS_PARAM_LEAF_COLOUR:
                flower.getVisualParameters().setLeafColour(tagContent);
                break;
            case VIS_PARAM_AVELENFLOWER:
                VisualParameters.AveLenFlower aLFlower = flower.getVisualParameters().getAveLenFlower();

                aLFlower.setAttribute(tagAttribute);
                aLFlower.setContent(tagContent);
                break;

            //Create and set <growingTips>
            case GRO_TIPS_TEMPERATURE:
                GrowingTips.Temperature temperature = flower.getGrowingTips().getTemperature();

                temperature.setAttribute(tagAttribute);
                temperature.setContent(tagContent);
                break;
            case GRO_TIPS_LIGHTING:
                GrowingTips.Lighting lighting = flower.getGrowingTips().getLighting();

                lighting.setAttribute(tagAttribute);
                break;
            case GRO_TIPS_WATERING:
                GrowingTips.Watering watering = flower.getGrowingTips().getWatering();

                watering.setContent(tagContent);
                watering.setAttribute(tagAttribute);
                break;
            default:
        }
    }
}