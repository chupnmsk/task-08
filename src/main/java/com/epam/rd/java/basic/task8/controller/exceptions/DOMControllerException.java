package com.epam.rd.java.basic.task8.controller.exceptions;

public class DOMControllerException extends Exception{
    public DOMControllerException (String message, Throwable cause){
        super(message,cause);
    }
}
