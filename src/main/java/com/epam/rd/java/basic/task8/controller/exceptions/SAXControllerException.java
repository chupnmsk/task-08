package com.epam.rd.java.basic.task8.controller.exceptions;

public class SAXControllerException extends Exception {
    public SAXControllerException(String message, Throwable cause) {
        super(message, cause);
    }
}