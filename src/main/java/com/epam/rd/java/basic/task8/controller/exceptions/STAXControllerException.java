package com.epam.rd.java.basic.task8.controller.exceptions;

public class STAXControllerException extends Exception{
    public STAXControllerException (String message, Throwable cause){
        super(message,cause);
    }
}
