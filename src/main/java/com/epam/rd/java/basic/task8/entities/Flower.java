package com.epam.rd.java.basic.task8.entities;

import java.util.Objects;

public class Flower {

    private String name;
    private String soil;
    private String origin;
    private String multiplying;

    private VisualParameters visualParameters = new VisualParameters();
    private GrowingTips growingTips = new GrowingTips();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSoil() {
        return soil;
    }

    public void setSoil(String soil) {
        this.soil = soil;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getMultiplying() {
        return multiplying;
    }

    public void setMultiplying(String multiplying) {
        this.multiplying = multiplying;
    }

    public VisualParameters getVisualParameters() {
        return visualParameters;
    }

    public void setVisualParameters(VisualParameters visualParameters) {
        this.visualParameters = visualParameters;
    }

    public GrowingTips getGrowingTips() {
        return growingTips;
    }

    public void setGrowingTips(GrowingTips growingTips) {
        this.growingTips = growingTips;
    }

    @Override
    public String toString() {
        return "Flower{" + '\n' +
                "name=" + name + '\n' +
                "soil=" + soil + '\n' +
                "origin=" + origin + '\n' +
                "multiplying=" + multiplying + '\n' +
                visualParameters + '\n' +
                growingTips + '\n' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Flower flower = (Flower) o;
        return Objects.equals(name, flower.name) && Objects.equals(soil, flower.soil) && Objects.equals(origin, flower.origin) && Objects.equals(multiplying, flower.multiplying) && Objects.equals(visualParameters, flower.visualParameters) && Objects.equals(growingTips, flower.growingTips);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, soil, origin, multiplying, visualParameters, growingTips);
    }
}
