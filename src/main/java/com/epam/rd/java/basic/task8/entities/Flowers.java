package com.epam.rd.java.basic.task8.entities;

import java.util.ArrayList;
import java.util.List;

public class Flowers extends Tag {

    private final List<Flower> childList = new ArrayList<>();

    public boolean addChild(Flower flower) {
        return childList.add(flower);
    }

    public List<Flower> getChildes() {
        return childList;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder(getAttribute() + "\n");
        for (Flower flower : childList){
            sb.append(flower).append("\n");
        }

        return sb.toString();
    }
}
