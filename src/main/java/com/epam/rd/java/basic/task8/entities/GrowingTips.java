package com.epam.rd.java.basic.task8.entities;

public class GrowingTips {

    private Temperature temperature = new Temperature();
    private Lighting lighting = new Lighting();
    private Watering watering = new Watering();

    public static class Temperature extends Tag {
    }

    public static class Lighting extends Tag {
    }

    public static class Watering extends Tag {
    }

    public Temperature getTemperature() {
        return temperature;
    }

    public void setTemperature(Temperature temperature) {
        this.temperature = temperature;
    }

    public Lighting getLighting() {
        return lighting;
    }

    public void setLighting(Lighting lighting) {
        this.lighting = lighting;
    }

    public Watering getWatering() {
        return watering;
    }

    public void setWatering(Watering watering) {
        this.watering = watering;
    }

}
