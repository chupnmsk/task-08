package com.epam.rd.java.basic.task8.entities;

import java.util.Comparator;

public class Sorter {

    public static final Comparator<Flower> sortByName = Comparator.comparing(Flower::getName);

    public static final Comparator<Flower> sortBySoilType = Comparator.comparing(Flower::getSoil);

    public static final Comparator<Flower> sortByAverageLength = (o1, o2) -> {
        Integer aveLenF1 = Integer.valueOf(o1.getVisualParameters().getAveLenFlower().getContent());
        Integer aveLenF2 = Integer.valueOf(o2.getVisualParameters().getAveLenFlower().getContent());

        return aveLenF1.compareTo(aveLenF2);
    };


    public static void setSortByName(Flowers root){
        root.getChildes().sort(sortByName);
    }

    public static void setSortBySoilType(Flowers root){
        root.getChildes().sort(sortBySoilType);
    }

    public static void setSortByAverageLength(Flowers root){
        root.getChildes().sort(sortByAverageLength);
    }

}