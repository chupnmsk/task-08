package com.epam.rd.java.basic.task8.entities;

public abstract class Tag {
    private String attribute;
    private String content;

    public String getAttribute() {
        return attribute;
    }

    public void setAttribute(String attribute) {
        this.attribute = attribute;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Override
    public String toString() {
        return  this.getClass().getName() + "{" +
                "attribute='" + attribute + '\'' +
                ", content='" + content + '\'' +
                '}';
    }
}
