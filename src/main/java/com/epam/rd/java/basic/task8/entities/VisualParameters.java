package com.epam.rd.java.basic.task8.entities;

public class VisualParameters {
    private String stemColour;
    private String leafColour;
    private AveLenFlower aveLenFlower = new AveLenFlower();

    public static class AveLenFlower extends Tag {
    }

    public String getStemColour() {
        return stemColour;
    }

    public void setStemColour(String stemColour) {
        this.stemColour = stemColour;
    }

    public String getLeafColour() {
        return leafColour;
    }

    public void setLeafColour(String leafColour) {
        this.leafColour = leafColour;
    }

    public AveLenFlower getAveLenFlower() {
        return aveLenFlower;
    }

    public void setAveLenFlower(AveLenFlower aveLenFlower) {
        this.aveLenFlower = aveLenFlower;
    }


    @Override
    public String toString() {
        String space = "                 ";
        return "VisualParameters{" + '\n' +
                space + "stemColour='" + stemColour + '\n' +
                space + "leafColour='" + leafColour + '\n' +
                space + "aveLenFlower=" + aveLenFlower + '\n' +
                '}';
    }
}

