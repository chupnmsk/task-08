package com.epam.rd.java.basic.task8.util;

public class XML {
    public static final String INPUT_XML = "input.xml";
    public static final String INPUT_XSD = "input.xsd";

    public static final String ROOT = "flowers";
    public static final String FLOWER_TAG = "flower";
    public static final String FLOWER_NAME = "name";
    public static final String FLOWER_SOIL = "soil";
    public static final String FLOWER_ORIGIN = "origin";
    public static final String FLOWER_MULTIPLYING = "multiplying";

    public static final String VISUAL_PARAMETERS_TAG = "visualParameters";
    public static final String VIS_PARAM_STEAM_COLOUR = "stemColour";
    public static final String VIS_PARAM_LEAF_COLOUR = "leafColour";
    public static final String VIS_PARAM_AVELENFLOWER = "aveLenFlower";

    public static final String GROWING_TIPS_TAG = "growingTips";
    public static final String GRO_TIPS_TEMPERATURE = "tempreture";
    public static final String GRO_TIPS_LIGHTING = "lighting";
    public static final String GRO_TIPS_WATERING = "watering";
}
