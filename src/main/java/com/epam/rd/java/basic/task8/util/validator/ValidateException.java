package com.epam.rd.java.basic.task8.util.validator;

public class ValidateException extends Exception {
    public ValidateException(String message, Throwable cause) {
        super(message,cause);
    }
}
