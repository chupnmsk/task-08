package com.epam.rd.java.basic.task8.util.validator;


import org.xml.sax.SAXException;

import javax.xml.XMLConstants;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import java.io.File;

import java.io.IOException;


public class ValidatorAgainstXSD {

    public static boolean validate(String xmlFileName, String xsdFileName) throws ValidateException {
        try {
        SchemaFactory factory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
            factory.setFeature("http://apache.org/xml/features/disallow-doctype-decl", true);
            factory.setProperty(XMLConstants.ACCESS_EXTERNAL_DTD, "");
            factory.setProperty(XMLConstants.ACCESS_EXTERNAL_SCHEMA, "");


        Schema schema = factory.newSchema(new File(xsdFileName));

        Validator validator = schema.newValidator();
        validator.validate(new StreamSource(xmlFileName));

        return true;
        } catch (SAXException | IOException ex) {
            throw new ValidateException(ex.getMessage(),ex);
        }
    }


}
