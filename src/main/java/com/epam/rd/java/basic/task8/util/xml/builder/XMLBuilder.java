package com.epam.rd.java.basic.task8.util.xml.builder;

import com.epam.rd.java.basic.task8.entities.*;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import java.io.File;
import java.util.LinkedHashMap;
import java.util.Map;

import static com.epam.rd.java.basic.task8.util.XML.*;

public class XMLBuilder {

    public static Document getDocument(Flowers flowers) throws XMLBuilderException {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
                factory.setNamespaceAware(true);
        Document document;
        try {
            factory.setFeature("http://apache.org/xml/features/disallow-doctype-decl", true);
            factory.setFeature("http://xml.org/sax/features/external-general-entities", false);

            DocumentBuilder documentBuilder = factory.newDocumentBuilder();
            document = documentBuilder.newDocument();
        } catch (ParserConfigurationException e) {
            throw new XMLBuilderException(e.getMessage(),e);
        }

        Element root = document.createElement(ROOT);

        setAttributes(root, flowers);

        document.appendChild(root);

        Element OuterTag;
        Element innerTag;
        for (Flower flower : flowers.getChildes()) {
            Element flowerElement = document.createElement(FLOWER_TAG);

            //set <name>
            OuterTag = document.createElement(FLOWER_NAME);
            OuterTag.setTextContent(flower.getName());
            flowerElement.appendChild(OuterTag);

            //set <soil>
            OuterTag = document.createElement(FLOWER_SOIL);
            OuterTag.setTextContent(flower.getSoil());
            flowerElement.appendChild(OuterTag);

            //set <origin>
            OuterTag = document.createElement(FLOWER_ORIGIN);
            OuterTag.setTextContent(flower.getOrigin());
            flowerElement.appendChild(OuterTag);

            //set <visualParameters>
            OuterTag = document.createElement(VISUAL_PARAMETERS_TAG);
            VisualParameters vParam = flower.getVisualParameters();

            innerTag = document.createElement(VIS_PARAM_STEAM_COLOUR);
            innerTag.setTextContent(vParam.getStemColour());
            OuterTag.appendChild(innerTag);

            innerTag = document.createElement(VIS_PARAM_LEAF_COLOUR);
            innerTag.setTextContent(vParam.getStemColour());
            OuterTag.appendChild(innerTag);

            innerTag = document.createElement(VIS_PARAM_AVELENFLOWER);
            setAttributes(innerTag, vParam.getAveLenFlower());
            innerTag.setTextContent(String.valueOf(vParam.getAveLenFlower().getContent()));
            OuterTag.appendChild(innerTag);

            flowerElement.appendChild(OuterTag);

            //set <growingTips>
            OuterTag = document.createElement(GROWING_TIPS_TAG);
            GrowingTips groTips = flower.getGrowingTips();

            innerTag = document.createElement(GRO_TIPS_TEMPERATURE);
            setAttributes(innerTag, groTips.getTemperature());
            innerTag.setTextContent(String.valueOf(groTips.getTemperature().getContent()));
            OuterTag.appendChild(innerTag);

            innerTag = document.createElement(GRO_TIPS_LIGHTING);
            setAttributes(innerTag, groTips.getLighting());
            OuterTag.appendChild(innerTag);

            innerTag = document.createElement(GRO_TIPS_WATERING);
            setAttributes(innerTag, groTips.getWatering());
            innerTag.setTextContent(String.valueOf(groTips.getWatering().getContent()));
            OuterTag.appendChild(innerTag);

            flowerElement.appendChild(OuterTag);

            //set <multiplying>
            OuterTag = document.createElement(FLOWER_MULTIPLYING);
            OuterTag.setTextContent(flower.getMultiplying());

            flowerElement.appendChild(OuterTag);

            root.appendChild(flowerElement);
        }
        return document;
    }

    static Map<String, String> getAttributeMap(String attribute) {
        Map<String, String> attributes = new LinkedHashMap<>();

        if (attribute == null) {
            return null;
        }

        String[] allAttr = attribute.split("\n");

        for (String s : allAttr) {
            String[] attr = s.split("=");

            String key = attr[0];
            String value = attr[1];

            attributes.put(key, value);
        }

        return attributes;
    }

    static void setAttributes(Element element, Tag entity) {
        Map<String, String> attributes = getAttributeMap(entity.getAttribute());
        if(attributes == null) {
            throw new IllegalArgumentException();
        }
            attributes.forEach(
                    (key, value) -> element.setAttribute(key,
                            value.replace("\"", "")));

    }

    public static void saveToXML(Document document, String xmlFileName) throws XMLBuilderException {
        StreamResult result = new StreamResult(new File(xmlFileName));
        try {
        TransformerFactory factory = TransformerFactory.newInstance();
            factory.setAttribute(XMLConstants.ACCESS_EXTERNAL_DTD, "");
            factory.setAttribute(XMLConstants.ACCESS_EXTERNAL_STYLESHEET, "");

        Transformer transformer = factory.newTransformer();
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");


        transformer.transform(new DOMSource(document), result);
        } catch (TransformerException e) {
            throw new XMLBuilderException(e.getMessage(),e);
        }
    }

}
