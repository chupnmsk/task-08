package com.epam.rd.java.basic.task8.util.xml.builder;

public class XMLBuilderException extends Exception{
    public XMLBuilderException(String message, Throwable cause){
        super(message,cause);
    }
}
